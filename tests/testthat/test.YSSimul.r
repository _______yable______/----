library(DiceKriging)
library(rgenoud)
library(parallel)
library(DiceOptim)

d <- 2; n <- 16
design.fact <- expand.grid(x1=seq(0,1,length=4), x2=seq(0,1,length=4))
y <- apply(design.fact, 1, branin)
m1 <- km(design=design.fact, response=y)

x1 = c(0.9616520, 0.15); x2 = c(0.1238946, 0.8166644); x3 = c(0.5427730, 0.15) #branin loc max
branin.pmax = list(x1,x2,x3)
branin.max = branin(x1)
branin.objs =seq(4,5,0.01)
testSim <- YSSimul$new(.kmMod=m1, .fun=branin, .objectives=sample(branin.objs, length(branin.objs)))
testSim$sortObj()
testSim$testFun()
testSim$testFun3()
# o<-testSim$maxEI(branin(x1),c(0,0),c(1,1))
# branin(o$par)
