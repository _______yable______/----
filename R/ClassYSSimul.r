#' Class for simulation
library(DiceKriging)
library(parallel)
library(rgenoud)
library(DiceOptim)
YSSimul <- setRefClass(
    "YSSimul", #class name
    fields=list(
        #class attributes
        fun = "function", # real computer experiment function
        doMax = "logical", # logical if true maximize distance, default false
        objectives = "array", # array of objectives
        kmMod = "km", #km-class story the design, response ,initial km.
        krigCtrl = "list", # kriging control parameter see DiceKriging::km
        eval.num = "numeric"
    ),

    methods=list(
        #class properties
        initialize = function(.kmMod ,.objectives, .krigCtrl=list(), .fun=DiceKriging::branin, .doMax=FALSE){
            # initializer
            fun <<- .fun #default 2 dim branin function
            doMax <<- .doMax # default find the minimum
            kmMod <<- .kmMod
            objectives <<- array(.objectives)
            krigCtrl <<- .krigCtrl
            eval.num <<- 0
        },
        sortObj = function(decreasing = FALSE){
            #print(objectives)
            objectives <<- array(sort(objectives, decreasing = decreasing))
            #print(objectives)
        },
        testFun = function(){
            #test function
            print("TEST START")
            print("TEST 1: TEST EI.grad")
            #test EI and EI.grad
            ei.step <- 1e-9
            x1 = c(0.9616520, 0.15); x2 = c(0.1238946, 0.8166644); x3 = c(0.5427730, 0.15) #branin loc min
            y = branin(x1)
            env <- new.env()
            step1 <- c(ei.step,0); step2 <- c(0,ei.step)
            ei.x1 <- EI(x1,y,envir=env)
            ei.x1.step1 <- EI(x1+step1,y)
            ei.x1.step2 <- EI(x1+step2,y)
            ei.grid.x1 <- EI.grad(x1,y,envir=env)
            print(paste("ei.x1=", as.character(ei.x1)))
            print(paste("ei.x1.step1=", as.character(ei.x1.step1)))
            print(paste("ei.x1.step2=", as.character(ei.x1.step2)))
            print(paste("analytic ei.grid.x1=", as.character(ei.grid.x1)))
            print(paste("simu ei.grid.x1=", as.character(c((ei.x1.step1-ei.x1)/ei.step, (ei.x1.step2-ei.x1)/ei.step))))

            #test maxEI
            print("TEST 2: test maxEI")
            o <- maxEI(y,c(0,0),c(1,1))
            print(o)
            #test ysEI
            print("TEST 3: test ysEI & ysEI.grad")
            envs <- apply(objectives,1,new.env)
            o <- ysEI(x1,envirs=envs)
            print(o)
            ov2 <- ysEIv2(x1,envir=env)
            print(ov2)
            o <- ysEI.grad(x1,envirs=envs)
            ov2 <- ysEIv2.grad(x1,envir=env)
            print(o)
            print(ov2)
            #test maxysEIv2
            print("TEST 4: test maxysEIv2")
            print(as.character(objectives))
#             o <- maxysEI(lower=c(0,0), upper=c(1,1))
#             print(o)
            o <- maxysEIv2(lower=c(0,0), upper=c(1,1))
            print(o)

        },

        testFun2 = function(){
            #test ysEGO
            print("TEST 5: test ysEGOv2")
            o <- ysEGOv2(epsilon=1e-4, lower=c(0,0), upper=c(1,1))
            print(o)
        },

        testFun3 = function(){
            #test EGO
            print("TEST 5: test EGO")
            o <- EGO(epsilon=1e-4, lower=c(0,0), upper=c(1,1))
            print(o)
        },

        EI = function(x,obj,plugin=NULL,type="UK", envir=NULL){
            #analytically compute Expected Improvement
            # plugin    optional scalar: if provided, it replaces the minimum of the current observations' distance to obj.
            #TODO(zys) test with obj=-inf
            if(is.null(plugin)){
                plugin <- min(abs(kmMod@y - obj))
            }
            b <- plugin
            d <- length(x)
            if (d != kmMod@d) {
                stop("x does not have the right size")
            }
            newdata.num <- as.numeric(x)
            newdata <- data.frame(t(newdata.num))
            colnames(newdata) = colnames(kmMod@X)
            predx <- predict(object = kmMod, newdata = newdata, type = type, checkNames = FALSE)
            m <- kriging.mean <- predx$mean
            s <- kriging.sd <- predx$sd
            a <- obj

            if (kriging.sd/sqrt(kmMod@covariance@sd2) < 1e-06) {
                #extremely low predict variance means too close to an evaluated design
                res <- 0
            }else{
                qlow <- (a-m-b)/s
                qhigh <- (a-m+b)/s
                qmid <- (a-m)/s
                res <- b*(pnorm(qhigh) - pnorm(qlow)) + (a-m)*(pnorm(qhigh) - 2*pnorm(qmid) + pnorm(qlow)) - s*(2*dnorm(qmid)-dnorm(qhigh)-dnorm(qlow))
            }

            if(!is.null(envir)){
                assign("predx",predx,envir=envir)
            }
            return(res)
        },

        EI.grad = function(x,obj,plugin=NULL,type="UK", envir=NULL){
            # analytically compute Expected Improvement experiment
            # plugin    optional scalar: if provided, it replaces the minimum of the current observations' distance to obj.
            if(is.null(plugin)){
                plugin <- min(abs(kmMod@y-obj))
            }
            b <- plugin
            a <- obj
            d <- length(x)
            if (d != kmMod@d) {
                stop("x does not have the right size")
            }
            newdata.num <- as.numeric(x)
            newdata <- data.frame(t(newdata.num))
            colnames(newdata) = colnames(kmMod@X)
            T <- kmMod@T    #t(T)*T = C
            X <- kmMod@X
            z <- kmMod@z    #inv(t(T))*(y - F*beta)
            u <- kmMod@M    #inv(t(T))*F
            covStruct <- kmMod@covariance

            if(!is.null(envir)){
                predx <- envir$predx
            }else{
                predx <- predict(object = kmMod, newdata = newdata, type = type, checkNames = FALSE, se.compute = TRUE, cov.compute = FALSE)
            }
            m <- kriging.mean <- predx$mean
            s <- kriging.sd <- predx$sd
            v <- predx$Tinv.c #an auxiliary vector, equal to T^(-1)*c
            c <- predx$c #an auxiliary matrix, containing all the covariances between new data and the initial design points.
            F.newdata <- model.matrix(kmMod@trend.formula, data = newdata)
            if (kriging.sd/sqrt(kmMod@covariance@sd2) < 1e-06) {
                ei.grad <- rep(0, d)
            }else{
                dc <- covVector.dx(x = newdata.num, X = X, object = covStruct, c = c) # \nabla c
                f.deltax <- trend.deltax(x = newdata.num, model = kmMod) # \nabla f
                W <- backsolve(t(T), dc, upper.tri = FALSE)
                m.grad <- kriging.mean.grad <- t(W) %*% z + t(kmMod@trend.coef %*% f.deltax)
                if (type == "UK") {
                    tuuinv <- solve(t(u) %*% u)
                    kriging.sd2.grad <- t(-2 * t(v) %*% W + 2 * (F.newdata - t(v) %*% u) %*% tuuinv %*% (f.deltax - t(t(W) %*% u)))
                }else{
                    kriging.sd2.grad <- t(-2 * t(v) %*% W)
                }
                s.grad <- kriging.sd.grad <- kriging.sd2.grad/(2 * kriging.sd)

                # here we have computed m.grad & s.grad
                ql = (a-b-m)/s; qm = (a-m)/s; qh = (a+b-m)/s
                pql = pnorm(ql); pqm = pnorm(qm); pqh = pnorm(qh)
                dql = dnorm(ql); dqm = dnorm(qm); dqh = dnorm(qh)
                ql.grad <- (-m.grad - ql * s.grad) / s
                qm.grad <- (-m.grad - qm * s.grad) / s
                qh.grad <- (-m.grad - qh * s.grad) / s

                ei.grad <- b*(dqh*qh.grad - dql*ql.grad) + (a - m.grad)*(pqh - 2*pqm + pql) + (a-m)*(dqh*qh.grad - 2*dqm*qm.grad + dql*ql.grad) - s.grad * (2*dqm - dqh - dql) + s*(2*dqm*qm*qm.grad - dqh*qh*qh.grad - dql*ql*ql.grad)
            }
            return(ei.grad)
        },

        maxEI = function(obj, lower, upper, plugin = NULL, type = "UK", parinit = NULL, control = NULL){
            if (is.null(plugin)){
                plugin <- min(abs(kmMod@y - obj))
            }
            ei.envir <- new.env()
            # TODO(zys) Notice here may cause error
            environment(EI) <- environment(EI.grad) <- ei.envir
            gr = EI.grad
            d <- ncol(kmMod@X)
            if (is.null(control$print.level))
                control$print.level <- 1
            if (d <= 6)
                N <- 3 * 2^d
            else N <- 32 * d
            if (is.null(control$BFGSmaxit))
                control$BFGSmaxit <- N
            if (is.null(control$pop.size))
                control$pop.size <- N
            if (is.null(control$solution.tolerance))
                control$solution.tolerance <- 1e-21
            if (is.null(control$max.generations))
                control$max.generations <- 12
            if (is.null(control$wait.generations))
                control$wait.generations <- 2
            if (is.null(control$BFGSburnin))
                control$BFGSburnin <- 2
            if (is.null(parinit))
                parinit <- lower + runif(d) * (upper - lower)
            domaine <- cbind(lower, upper)

            o <- genoud(EI, nvars = d, max = TRUE, pop.size = control$pop.size,
                max.generations = control$max.generations, wait.generations = control$wait.generations,
                hard.generation.limit = TRUE, starting.values = parinit,
                MemoryMatrix = TRUE, Domains = domaine, default.domains = 10,
                solution.tolerance = control$solution.tolerance, gr = gr,
                boundary.enforcement = 2, lexical = FALSE, gradient.check = FALSE,
                BFGS = TRUE, data.type.int = FALSE, hessian = FALSE,
                unif.seed = floor(runif(1, max = 10000)), int.seed = floor(runif(1, max = 10000)), print.level = control$print.level,
                share.type = 0, instance.number = 0, output.path = "stdout",
                output.append = FALSE, project.path = NULL, P1 = 50,
                P2 = 50, P3 = 50, P4 = 50, P5 = 50, P6 = 50, P7 = 50,
                P8 = 50, P9 = 0, P9mix = NULL, BFGSburnin = control$BFGSburnin,
                BFGSfn = NULL, BFGShelp = NULL, control = list(maxit = control$BFGSmaxit),
                cluster = FALSE, balance = FALSE, debug = FALSE, obj=obj,
                plugin = plugin, type = type,
                envir = ei.envir)

            o$par <- t(as.matrix(o$par))
            colnames(o$par) <- colnames(kmMod@X)
            o$value <- as.matrix(o$value)
            colnames(o$value) <- "EI"
            return(list(par = o$par, value = o$value))
        },

        ysEI = function(x, objs=objectives, plugin=NULL,type="UK", envirs=NULL){
            #TODO(zys) we can also add a weight
            #envirs environment list
            #plugin plugin array

            if(is.null(envirs)){
                res <- sum(apply(cbind(objs, 1:length(objs)),1,function(o) EI(x,o[1],plugin=plugin[o[2]],type=type)))
            }else{
                if((!is.list(envirs))||(!is.environment(envirs[[1]]))){
                    stop("envirs must be environment list")
                }else{
                    res <- sum(apply(cbind(objs, 1:length(objs)), 1, function(a) EI(x, a[1], plugin=plugin[a[2]], type=type, envir=envirs[[a[2]]])))
                }
            }
            return(res)
        },

        ysEIv2 = function(x, objs=objectives, weight=NULL, plugins=NULL, type="UK", envir=NULL){
            y <- kmMod@y
            if(is.null(plugins)){
                plugins = apply(objs, 1, function(x) min(abs(y - x)))
            }
            b <- plugins
            d <- length(x)
            if (d != kmMod@d) {
                stop("x does not have the right size")
            }
            if (is.null(weight)){
                weight <- rep(1/length(objs), length(objs)) #weight
            }
            newdata.num <- as.numeric(x)
            newdata <- data.frame(t(newdata.num))
            colnames(newdata) = colnames(kmMod@X)
            predx <- predict(object = kmMod, newdata = newdata, type = type, checkNames = FALSE)
            m <- kriging.mean <- predx$mean
            s <- kriging.sd <- predx$sd
            a <- objs

            if (kriging.sd/sqrt(kmMod@covariance@sd2) < 1e-06) {
                #extremely low predict variance means too close to an evaluated design
                res <- 0
            }else{
                qlow <- (a-m-b)/s
                qhigh <- (a-m+b)/s
                qmid <- (a-m)/s
                res <- b*(pnorm(qhigh) - pnorm(qlow)) + (a-m)*(pnorm(qhigh) - 2*pnorm(qmid) + pnorm(qlow)) - s*(2*dnorm(qmid)-dnorm(qhigh)-dnorm(qlow))
                res <- (weight %*% res)[1,1]
            }

            if(!is.null(envir)){
                assign("predx",predx,envir=envir)
            }
            return(res)
        },

        ysEI.grad = function(x, objs=objectives, plugin=NULL,type="UK", envirs=NULL){
            #TODO(zys) we can also add a weight
            if(is.null(envirs)){
                res <- sum(apply(cbind(objs, 1:length(objs)),1,function(o) EI.grad(x,o[1],plugin=plugin[o[2]],type=type)))
            }else{
                if((!is.list(envirs))||(!is.environment(envirs[[1]]))){
                    stop("envirs must be environment list")
                }else{
                    res <- rowSums(apply(cbind(objs, 1:length(objs)), 1, function(a) EI.grad(x, a[1], plugin=plugin[a[2]], type=type, envir=envirs[[a[2]]])))
                }
            }
            return(res)
        },

        ysEIv2.grad = function(x, objs=objectives, weight=NULL, plugins=NULL,type="UK", envir=NULL){
            y <- kmMod@y
            if(is.null(plugins)){
                plugins = apply(objs, 1, function(x) min(abs(y - x)))
            }
            b <- plugins
            d <- length(x)
            a <-objs
            if (d != kmMod@d) {
                stop("x does not have the right size")
            }
            if (is.null(weight)){
                weight <- rep(1/length(objs), length(objs)) #weight
            }
            newdata.num <- as.numeric(x)
            newdata <- data.frame(t(newdata.num))
            colnames(newdata) = colnames(kmMod@X)
            T <- kmMod@T    #t(T)*T = C
            X <- kmMod@X
            z <- kmMod@z    #inv(t(T))*(y - F*beta)
            u <- kmMod@M    #inv(t(T))*F
            covStruct <- kmMod@covariance
            if(!is.null(envir)){
                predx <- envir$predx
            }else{
                predx <- predict(object = kmMod, newdata = newdata, type = type, checkNames = FALSE, se.compute = TRUE, cov.compute = FALSE)
            }
            m <- kriging.mean <- predx$mean
            s <- kriging.sd <- predx$sd
            v <- predx$Tinv.c #an auxiliary vector, equal to T^(-1)*c
            c <- predx$c #an auxiliary matrix, containing all the covariances between new data and the initial design points.
            F.newdata <- model.matrix(kmMod@trend.formula, data = newdata)
            if (kriging.sd/sqrt(kmMod@covariance@sd2) < 1e-06) {
                ei.grad <- rep(0, d)
            }else{
                dc <- covVector.dx(x = newdata.num, X = X, object = covStruct, c = c) # \nabla c
                f.deltax <- trend.deltax(x = newdata.num, model = kmMod) # \nabla f
                W <- backsolve(t(T), dc, upper.tri = FALSE)
                m.grad <- kriging.mean.grad <- t(W) %*% z + t(kmMod@trend.coef %*% f.deltax)
                if (type == "UK") {
                    tuuinv <- solve(t(u) %*% u)
                    kriging.sd2.grad <- t(-2 * t(v) %*% W + 2 * (F.newdata - t(v) %*% u) %*% tuuinv %*% (f.deltax - t(t(W) %*% u)))
                }else{
                    kriging.sd2.grad <- t(-2 * t(v) %*% W)
                }
                s.grad <- kriging.sd.grad <- kriging.sd2.grad/(2 * kriging.sd)

                # here we have computed m.grad & s.grad
                ql = (a-b-m)/s; qm = (a-m)/s; qh = (a+b-m)/s
                pql = pnorm(ql); pqm = pnorm(qm); pqh = pnorm(qh)
                dql = dnorm(ql); dqm = dnorm(qm); dqh = dnorm(qh)
                #TODO(zys): modify here
                ql.grad <- apply(-s.grad %*% ql, 2, function(x) x-m.grad)/s
                qm.grad <- apply(-s.grad %*% qm, 2, function(x) x-m.grad)/s
                qh.grad <- apply(-s.grad %*% qh, 2, function(x) x-m.grad)/s

                dqhMqh.grad <- t(apply(qh.grad,1,function(x) x*dqh))
                dqlMql.grad <- t(apply(ql.grad,1,function(x) x*dql))
                dqmMqm.grad <- t(apply(qm.grad,1,function(x) x*dqm))

                #ei.grad <- b*(dqh*qh.grad - dql*ql.grad) + (a - m.grad)*(pqh - 2*pqm + pql) + (a-m)*(dqh*qh.grad - 2*dqm*qm.grad + dql*ql.grad) - s.grad * (2*dqm - dqh - dql) + s*(2*dqm*qm*qm.grad - dqh*qh*qh.grad - dql*ql*ql.grad)
                ei.grad <- t(apply(dqhMqh.grad - dqlMql.grad, 1, function(x) x*b)) + t(apply(-m.grad, 1, function(x) (x+a)*(pqh - 2*pqm + pql))) + t(apply(dqhMqh.grad - 2*dqmMqm.grad + dqlMql.grad, 1, function(x) x*(a-m))) - s.grad %*% (2*dqm - dqh - dql) + s*(2*t(apply(dqmMqm.grad,1,function(x) x*qm)) - t(apply(dqhMqh.grad,1,function(x) x*qh)) - t(apply(dqlMql.grad,1,function(x) x*ql)))
                ei.grad <- ei.grad %*% weight
            }
            return(ei.grad)

        },

        maxysEI = function(objs=objectives, lower, upper, plugin = NULL, type = "UK", parinit = NULL, control = NULL){
            if (is.null(plugin)){
                plugin <- apply(objs,1,function(x) min(abs(kmMod@y - x)))
            }
            ysei.envirs <- apply(objs,1,new.env)
            environment(ysEI) <- environment(ysEI.grad) <- new.env() #share the same environment
            gr = ysEI.grad
            d <- ncol(kmMod@X)
            if (is.null(control$print.level))
                control$print.level <- 1
            if (d <= 6)
                N <- 3 * 2^d
            else N <- 32 * d
            if (is.null(control$BFGSmaxit))
                control$BFGSmaxit <- N
            if (is.null(control$pop.size))
                control$pop.size <- N
            if (is.null(control$solution.tolerance))
                control$solution.tolerance <- 1e-21
            if (is.null(control$max.generations))
                control$max.generations <- 12
            if (is.null(control$wait.generations))
                control$wait.generations <- 2
            if (is.null(control$BFGSburnin))
                control$BFGSburnin <- 2
            if (is.null(parinit))
                parinit <- lower + runif(d) * (upper - lower)
            domaine <- cbind(lower, upper)

            o <- genoud(ysEI, nvars = d, max = TRUE, pop.size = control$pop.size,
                max.generations = control$max.generations, wait.generations = control$wait.generations,
                hard.generation.limit = TRUE, starting.values = parinit,
                MemoryMatrix = TRUE, Domains = domaine, default.domains = 10,
                solution.tolerance = control$solution.tolerance, gr = gr,
                boundary.enforcement = 2, lexical = FALSE, gradient.check = FALSE,
                BFGS = TRUE, data.type.int = FALSE, hessian = FALSE,
                unif.seed = floor(runif(1, max = 10000)), int.seed = floor(runif(1, max = 10000)), print.level = control$print.level,
                share.type = 0, instance.number = 0, output.path = "stdout",
                output.append = FALSE, project.path = NULL, P1 = 50,
                P2 = 50, P3 = 50, P4 = 50, P5 = 50, P6 = 50, P7 = 50,
                P8 = 50, P9 = 0, P9mix = NULL, BFGSburnin = control$BFGSburnin,
                BFGSfn = NULL, BFGShelp = NULL, control = list(maxit = control$BFGSmaxit),
                cluster = FALSE, balance = FALSE, debug = FALSE, objs=objs,
                plugin = plugin, type = type,
                envirs = ysei.envirs)

                o$par <- t(as.matrix(o$par))
                colnames(o$par) <- colnames(kmMod@X)
                o$value <- as.matrix(o$value)
                colnames(o$value) <- "ysEI"
                return(list(par = o$par, value = o$value))
        },

        maxysEIv2 = function(objs=objectives, lower, upper, plugins = NULL, type = "UK", parinit = NULL, control = NULL){
            if (is.null(plugins)){
                plugins <- apply(objs,1,function(x) min(abs(kmMod@y - x)))
            }
            ei.envir <- new.env()
            #environment(ysEIv2) <- environment(ysEIv2.grad) <- ei.envir
            gr = ysEIv2.grad
            d <- ncol(kmMod@X)
            if (is.null(control$print.level))
                control$print.level <- 1
            if (d <= 6)
                N <- 3 * 2^d
            else N <- 32 * d
            if (is.null(control$BFGSmaxit))
                control$BFGSmaxit <- N
            if (is.null(control$pop.size))
                control$pop.size <- N
            if (is.null(control$solution.tolerance))
                control$solution.tolerance <- 1e-21
            if (is.null(control$max.generations))
                control$max.generations <- 12
            if (is.null(control$wait.generations))
                control$wait.generations <- 2
            if (is.null(control$BFGSburnin))
                control$BFGSburnin <- 2
            if (is.null(parinit))
                parinit <- lower + runif(d) * (upper - lower)
            domaine <- cbind(lower, upper)
            o <- genoud(ysEIv2, nvars = d, max = TRUE, pop.size = control$pop.size,
                max.generations = control$max.generations, wait.generations = control$wait.generations,
                hard.generation.limit = TRUE, starting.values = parinit,
                MemoryMatrix = TRUE, Domains = domaine, default.domains = 10,
                solution.tolerance = control$solution.tolerance, gr = gr,
                boundary.enforcement = 2, lexical = FALSE, gradient.check = FALSE,
                BFGS = TRUE, data.type.int = FALSE, hessian = FALSE,
                unif.seed = floor(runif(1, max = 10000)), int.seed = floor(runif(1, max = 10000)), print.level = control$print.level,
                share.type = 0, instance.number = 0, output.path = "stdout",
                output.append = FALSE, project.path = NULL, P1 = 50,
                P2 = 50, P3 = 50, P4 = 50, P5 = 50, P6 = 50, P7 = 50,
                P8 = 50, P9 = 0, P9mix = NULL, BFGSburnin = control$BFGSburnin,
                BFGSfn = NULL, BFGShelp = NULL, control = list(maxit = control$BFGSmaxit),
                cluster = FALSE, balance = FALSE, debug = FALSE, objs=objs,
                plugins = plugins, type = type,
                envir = ei.envir)

            o$par <- t(as.matrix(o$par))
            colnames(o$par) <- colnames(kmMod@X)
            o$value <- as.matrix(o$value)
            colnames(o$value) <- "EI"
            return(list(par = o$par, value = o$value))

        },

        ysEGO = function(.fun=fun, epsilon, nstep=NULL, lower, upper, parinit=NULL, control=NULL, kmcontrol=NULL){
			objs <- objectives
            n <- nrow(kmMod@X)
			d <- ncol(kmMod@X)
            if (is.null(kmcontrol$penalty))
                kmcontrol$penalty <- kmMod@penalty
            if (length(kmMod@penalty == 0))
                kmcontrol$penalty <- NULL
            if (is.null(kmcontrol$optim.method))
                kmcontrol$optim.method <- kmMod@optim.method
            if (is.null(kmcontrol$parinit))
                kmcontrol$parinit <- kmMod@parinit
            if (is.null(kmcontrol$control))
                kmcontrol$control <- kmMod@control
            #TODO(zys) modified codes blow
			if(is.null(control$solution.tolerance))
				control$solution.tolerance <- max(epsilon,1e-12)
			if(is.null(control$print.level))
			  control$print.level <- 0
			eval.times = 0
			res.design <- matrix(ncol=d)
			res.design <- res.design[-1,]
			res.obj <- matrix(ncol=1)
			res.obj <-res.obj[-1,]
			res.response <- matrix(ncol=1)
			res.response <- res.response[-1,]

            while((is.null(nstep))||(eval.times < nstep)){
				eval.times <- eval.times + 1
                oEGO <- maxysEI(objs=objs, lower = lower, upper = upper, parinit = parinit, control = control)
				new.design   <- oEGO$par
				new.response <- .fun(t(new.design))
                kmMod@X <<- rbind(kmMod@X, new.design)
                kmMod@y <<- rbind(kmMod@y, new.response)
                kmcontrol$parinit <- covparam2vect(kmMod@covariance)
                kmcontrol$control$trace = FALSE
                if (kmMod@param.estim){
                    kmMod <<- km(formula = kmMod@trend.formula, design = kmMod@X,
                    response = kmMod@y, covtype = kmMod@covariance@name,
                    lower = kmMod@lower, upper = kmMod@upper, nugget = NULL,
                    penalty = kmcontrol$penalty, optim.method = kmcontrol$optim.method,
                    parinit = kmcontrol$parinit, control = kmcontrol$control,
                    gr = kmMod@gr, iso = is(kmMod@covariance, "covIso"))
                }
                else {
                    coef.cov <- covparam2vect(kmMod@covariance)
                    kmMod <<- km(formula = kmMod@trend.formula, design = kmMod@X,
                    response = kmMod@y, covtype = kmMod@covariance@name,
                    coef.trend = kmMod@trend.coef, coef.cov = coef.cov,
                    coef.var = kmMod@covariance@sd2, nugget = NULL,
                    iso = is(kmMod@covariance, "covIso"))
                }
				objs.distance <- abs(objs - new.response)
				objs.ok <- (objs.distance < epsilon)
				#DEBUG
				print(paste("evaluated num: ", as.character(eval.times), as.character(new.response)))
				#
				if(any(objs.ok)){
					objs.index <- match(TRUE, objs.ok)
					res.design <- rbind(res.design, new.design)
					res.obj <- rbind(res.obj,objs[objs.index])
					res.response <- rbind(res.response, new.response)
					objs <- objs[-objs.index]

					#DEBUG
					print(paste("Unfinished objs num:	", as.character(length(objs))))
					#
					if(length(objs)==0){
						break
					}
				}
            }
			eval.num <<- eval.times
			return(list(design=res.design, response=res.response, objective=res.obj, epsilon=epsilon, nstep=nstep, eval.times=eval.times))

        },
        ysEGOv2 = function(.fun=fun, epsilon, nstep=NULL, lower, upper, parinit=NULL, control=NULL, kmcontrol=NULL){
			objs <- objectives
            n <- nrow(kmMod@X)
			d <- ncol(kmMod@X)
            if (is.null(kmcontrol$penalty))
                kmcontrol$penalty <- kmMod@penalty
            if (length(kmMod@penalty == 0))
                kmcontrol$penalty <- NULL
            if (is.null(kmcontrol$optim.method))
                kmcontrol$optim.method <- kmMod@optim.method
            if (is.null(kmcontrol$parinit))
                kmcontrol$parinit <- kmMod@parinit
            if (is.null(kmcontrol$control))
                kmcontrol$control <- kmMod@control
            #TODO(zys) modified codes blow
			if(is.null(control$solution.tolerance))
				control$solution.tolerance <- max(epsilon,1e-12)
			if(is.null(control$print.level))
			  control$print.level <- 0
			eval.times = 0
			res.design <- matrix(ncol=d)
			res.design <- res.design[-1,]
			res.obj <- matrix(ncol=1)
			res.obj <-res.obj[-1,]
			res.response <- matrix(ncol=1)
			res.response <- res.response[-1,]

            while((is.null(nstep))||(eval.times < nstep)){
				eval.times <- eval.times + 1
                oEGO <- maxysEIv2(objs=objs, lower = lower, upper = upper, parinit = parinit, control = control)
				new.design   <- oEGO$par
				new.response <- .fun(t(new.design))
                kmMod@X <<- rbind(kmMod@X, new.design)
                kmMod@y <<- rbind(kmMod@y, new.response)
                kmcontrol$parinit <- covparam2vect(kmMod@covariance)
                kmcontrol$control$trace = FALSE
                if (kmMod@param.estim){
                    kmMod <<- km(formula = kmMod@trend.formula, design = kmMod@X,
                    response = kmMod@y, covtype = kmMod@covariance@name,
                    lower = kmMod@lower, upper = kmMod@upper, nugget = NULL,
                    penalty = kmcontrol$penalty, optim.method = kmcontrol$optim.method,
                    parinit = kmcontrol$parinit, control = kmcontrol$control,
                    gr = kmMod@gr, iso = is(kmMod@covariance, "covIso"))
                }
                else {
                    coef.cov <- covparam2vect(kmMod@covariance)
                    kmMod <<- km(formula = kmMod@trend.formula, design = kmMod@X,
                    response = kmMod@y, covtype = kmMod@covariance@name,
                    coef.trend = kmMod@trend.coef, coef.cov = coef.cov,
                    coef.var = kmMod@covariance@sd2, nugget = NULL,
                    iso = is(kmMod@covariance, "covIso"))
                }
				objs.distance <- abs(objs - new.response)
				objs.ok <- (objs.distance < epsilon)
				#DEBUG
				print(paste("evaluated num: ", as.character(eval.times), as.character(new.response)))
				#
				if(any(objs.ok)){
					objs.index <- match(TRUE, objs.ok)
					res.design <- rbind(res.design, new.design)
					res.obj <- rbind(res.obj,objs[objs.index])
					res.response <- rbind(res.response, new.response)
					objs <- objs[-objs.index]

					#DEBUG
					print(paste("Unfinished objs num: ", as.character(length(objs))))
					#
					if(length(objs)==0){
						break
					}
				}
            }
			eval.num <<- eval.times
			return(list(design=res.design, response=res.response, objective=res.obj, epsilon=epsilon, nstep=nstep, eval.times=eval.times))
        },
        EGO = function(.fun=fun, epsilon, nstep=NULL, lower, upper, parinit=NULL, control=NULL, kmcontrol=NULL){
            objs <- objectives
            n <- nrow(kmMod@X)
			d <- ncol(kmMod@X)
            if (is.null(kmcontrol$penalty))
                kmcontrol$penalty <- kmMod@penalty
            if (length(kmMod@penalty == 0))
                kmcontrol$penalty <- NULL
            if (is.null(kmcontrol$optim.method))
                kmcontrol$optim.method <- kmMod@optim.method
            if (is.null(kmcontrol$parinit))
                kmcontrol$parinit <- kmMod@parinit
            if (is.null(kmcontrol$control))
                kmcontrol$control <- kmMod@control
            #TODO(zys) modified codes blow
			if(is.null(control$solution.tolerance))
				control$solution.tolerance <- max(epsilon,1e-12)
			if(is.null(control$print.level))
			  control$print.level <- 0
			eval.times = 0
			res.design <- matrix(ncol=d)
			res.design <- res.design[-1,]
			res.obj <- matrix(ncol=1)
			res.obj <-res.obj[-1,]
			res.response <- matrix(ncol=1)
			res.response <- res.response[-1,]

			while((is.null(nstep))||(eval.times < nstep)){
			    eval.times <- eval.times + 1
			    oEGO <- maxEI(obj=objs[1], lower = lower, upper = upper, parinit = parinit, control = control)
			    new.design   <- oEGO$par
				new.response <- .fun(t(new.design))
                kmMod@X <<- rbind(kmMod@X, new.design)
                kmMod@y <<- rbind(kmMod@y, new.response)
                kmcontrol$parinit <- covparam2vect(kmMod@covariance)
                kmcontrol$control$trace = FALSE
                if (kmMod@param.estim){
                    kmMod <<- km(formula = kmMod@trend.formula, design = kmMod@X,
                    response = kmMod@y, covtype = kmMod@covariance@name,
                    lower = kmMod@lower, upper = kmMod@upper, nugget = NULL,
                    penalty = kmcontrol$penalty, optim.method = kmcontrol$optim.method,
                    parinit = kmcontrol$parinit, control = kmcontrol$control,
                    gr = kmMod@gr, iso = is(kmMod@covariance, "covIso"))
                }
                else {
                    coef.cov <- covparam2vect(kmMod@covariance)
                    kmMod <<- km(formula = kmMod@trend.formula, design = kmMod@X,
                    response = kmMod@y, covtype = kmMod@covariance@name,
                    coef.trend = kmMod@trend.coef, coef.cov = coef.cov,
                    coef.var = kmMod@covariance@sd2, nugget = NULL,
                    iso = is(kmMod@covariance, "covIso"))
                }
                objs.distance <- abs(objs - new.response)
				objs.ok <- (objs.distance < epsilon)
				#DEBUG
				print(paste("evaluated num: ", as.character(eval.times), as.character(new.response), as.character(objs[1])))
				#
				if(any(objs.ok)){
					objs.index <- match(TRUE, objs.ok)
					res.design <- rbind(res.design, new.design)
					res.obj <- rbind(res.obj,objs[objs.index])
					res.response <- rbind(res.response, new.response)
					objs <- objs[-objs.index]

					#DEBUG
					print(paste("Unfinished objs num:	", as.character(length(objs))))
					#
					if(length(objs)==0){
						break
					}
				}
			}
			eval.num <<- eval.times
			return(list(design=res.design, response=res.response, objective=res.obj, epsilon=epsilon, nstep=nstep, eval.times=eval.times))
        }


    )
)
