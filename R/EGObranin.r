#! /usr/bin/Rscript
#' example using EGO to 2 dimensional branin function


library('DiceKriging')
library('DiceOptim')

d <- 2; n <- 15
set.seed(0); design <- optimumLHS(n, d)
design <- data.frame(design); names(design) <- c("x1", "x2")
response.branin <- apply(design, 1, branin)
fitted.model1 <- km(design = design, response = response.branin)
x.grid <- y.grid <- seq(0, 1, length = n.grid <- 25)
design.grid <- expand.grid(x.grid, y.grid)
EI.grid <- apply(design.grid, 1, EI, fitted.model1)



nsteps <- 10
lower <- rep(0, d); upper <- rep(1, d)
oEGO <- EGO.nsteps(model = fitted.model1, fun = branin, nsteps = nsteps, lower, upper, control = list(pop.size = 20, BFGSburnin = 2))
par(mfrow = c(1, 2))
response.grid <- apply(design.grid,1,branin)
z.grid <- matrix(response.grid, n.grid, n.grid)
contour(x.grid, y.grid, z.grid, 40)
points(design[ , 1], design[ , 2], pch = 17, col = "blue")
points(oEGO$par, pch = 19, col = "red")
text(oEGO$par[ , 1], oEGO$par[ , 2], labels = 1:nsteps, pos = 3)
EI.grid <- apply(design.grid, 1, EI, oEGO$lastmodel)
z.grid <- matrix(EI.grid, n.grid, n.grid)
contour(x.grid, y.grid, z.grid, 20)
points(design[ , 1], design[ , 2], pch = 17, col = "blue")
points(oEGO$par, pch = 19, col = "red")

integrate(function(x) x*dnorm(x),1,3)
intnorm = function(l,h){
    1/sqrt(2*pi) *(exp(-l^2/2) - exp(-h^2/2))
}
